import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
// If you want to use Vuex, a state management pattern https://vuex.vuejs.org/
export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
