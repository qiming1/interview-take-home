import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {

  // these are the currently used company colors, most notably primary purple
  theme: {
    primary: '#432B59',
    secondary: '#F2F2F2',
    accent: '#EC9E92',
    error: '#D1001A',
    success: '#23947A',
    warning: '#FFC107'
  },
  iconfont: 'md'
});
